# TCP packet drop analysis

## Introduction

This project consists in the replication of 007, 
a packet drop cause detection system proposed by [Arzani et al. (2018)](https://www.usenix.org/system/files/conference/nsdi18/nsdi18-arzani.pdf).

A lightweight system that sits outside the network, 007 was introduced to answer
the question of how to detect the sources of TCP packet drops in a large datacenter.
It is scalable to datacenter size networks, and can be deployed in running datacenters with little change to the infrastructure.

## Design

The system consists of 3 distributed agents ```- Monitoring, discovery, and analysis - ``` as shown in the figure below.
In our implementation:

1. The monitoring agent uses “tshark”, a command line version of Wireshark, to pipe network activity to a java monitoring program.
When the monitoring program detects a packet drop, extract source and destination and send to discovery agent.
2. The discovery agent with a probability of 50% chooses between a Breath-Fisrt  or Depth-First Search to find the route between two nodes in the given network topology.
Once the path is found, it is stored and transmitted to the analysis agent every 30 seconds. Transmitting every 30 seconds allows memory efficiency.
3. Upon reception of the paths where a packet was dropped, the analysis agent applies a penalty to every link on every path,
and keeps track of the worst link in a given period. By default every link is initialized with 0 as penalty,
but assumed to be equally responsible of the drop as every other link on the path,
every link on a retransmitted path is assigned 1/h penalty cost for every appearance. h denotes the number of hops on the path.
This approach of marking worse links is extended to switches on the path, hence the worst switch is detected as well.

![architecture](images/architecture.png)


## Experiments

In order to verify the replicated system, we proposed a network simulation envorinment where linear or ring topologies can be used to run simulations.
An instance is run and recorded in the video below.

![demo](images/demo.mp4)

## Summary

Experiments have proved that our implementation of the 007 program was able to discover bad links and switches on the network in every test.
Though we were not able to replicate the exact results as in the research paper, due to limited resources,
we believe that our implementation proves that 007 can efficiently detect TCP drops causes on any given network.


## Reference
Arzani, B., Ciraci, S., Chamon, L., Zhu, Y., Liu, H. H., Padhye, J., Loo, B. T.,
Outhred, G. (2018). 007: Democratically finding the cause of packet drops. In 15th USENIX Symposium on Networked Systems Design and Implementation (NSDI 18).(pp. 419-435).